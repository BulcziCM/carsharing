﻿//using CarSharing.BusinessLayer.Services;
//using CarSharing.DataLayer.Models;
//using FluentAssertions;
//using Moq;
//using NUnit.Framework;

//namespace CarSharing.BusinessLayer.Tests
//{
//    public class DiscountCalculatorTests
//    {
//        [Test]
//        public void UserIsNotVip_Returns0()
//        {
//            var calculator = new DiscountCalculator(null);

//            var user = new User { IsVip = false };
//            double discount = calculator.GetDiscount(user).Result;

//            discount.Should().Be(0);
//        }

//        [Test]
//        public void UserIsVip_ReturnsMoreThan0()
//        {
//            var configProviderMock = new Mock<IConfigProvider>();
//            configProviderMock.Setup(p => p.Get(It.IsAny<string>()))
//                .Returns(0.3);
//            var calculator = new DiscountCalculator(configProviderMock.Object);

//            var user = new User { IsVip = true };
//            double discount = calculator.GetDiscount(user).Result;

//            discount.Should().BeGreaterThan(0);
//        }

//        [Test]
//        public void UserIsVip_ConfigProviderIsCalledWithCorrectArgument()
//        {
//            var configProviderMock = new Mock<IConfigProvider>();
//            var calculator = new DiscountCalculator(configProviderMock.Object);

//            var user = new User { IsVip = true };
//            double discount = calculator.GetDiscount(user).Result;

//            configProviderMock.Verify(p => p.Get("VipDiscount"));
//        }

//        [Test]
//        public void UserIsNotVip_ConfigProviderIsNotCalled()
//        {
//            var configProviderMock = new Mock<IConfigProvider>();
//            var calculator = new DiscountCalculator(configProviderMock.Object);

//            var user = new User { IsVip = false };
//            double discount = calculator.GetDiscount(user).Result;

//            configProviderMock.Verify(p => p.Get(It.IsAny<string>()),
//                Times.Never);
//        }
//    }
//}