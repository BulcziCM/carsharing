﻿//using System;
//using System.Threading.Tasks;
//using CarSharing.BusinessLayer.Services;
//using CarSharing.DataLayer.Models;
//using FluentAssertions;
//using Moq;
//using NUnit.Framework;

//namespace CarSharing.BusinessLayer.Tests
//{
//    public class PriceCalculationServiceTests
//    {
//        [Test]
//        public void RentalDistanceIs0AndTimeIs0AndPricePerDayIs100_Returns100()
//        {
//            Mock<IDiscountCalculator> discountCalculatorMock = new Mock<IDiscountCalculator>();
//            discountCalculatorMock
//                .Setup(calculator => calculator.GetDiscount(It.IsAny<User>()))
//                .Returns(Task.FromResult(0.0d));

//            var service = new PriceCalculationService(discountCalculatorMock.Object);
//            var rental = new Rental
//            {
//                StartDate = new DateTime(2014, 10, 18),
//                EndDate = new DateTime(2014, 10, 18),
//                Distance = 0f,
//                Car = new Car
//                {
//                    PricePerKM = 10000f,
//                    PricePerDay = 100f
//                }
//            };

//            double totalPrice = service.GetRentalCost(rental);

//            totalPrice.Should().Be(100);
//        }

//        [TestCase(50, 2, 100)]
//        [TestCase(0, 9999, 0)]
//        [TestCase(9999, 0, 0)]
//        [TestCase(1000, 1000, 1000000)]
//        [TestCase(40f, 2.5f, 100)]
//        public void PricePerDayIs0_ReturnsDistanceTimesPricePerKm
//            (float distance, float pricePerKm, float expectedPrice)
//        {
//            Mock<IDiscountCalculator> discountCalculatorMock = new Mock<IDiscountCalculator>();
//            discountCalculatorMock
//                .Setup(calculator => calculator.GetDiscount(It.IsAny<User>()))
//                .Returns(Task.FromResult(0.0d));

//            var service = new PriceCalculationService(discountCalculatorMock.Object);
//            var rental = new Rental
//            {
//                StartDate = new DateTime(2014, 10, 18),
//                EndDate = new DateTime(2024, 10, 18),
//                Distance = distance,
//                Car = new Car
//                {
//                    PricePerKM = pricePerKm,
//                    PricePerDay = 0f
//                }
//            };

//            double totalPrice = service.GetRentalCost(rental);

//            totalPrice.Should().Be(expectedPrice);
//        }

//        [Test]
//        public void RentalIsNull_ThrowsArgumentNullException()
//        {
//            Mock<IDiscountCalculator> discountCalculatorMock = new Mock<IDiscountCalculator>();
//            discountCalculatorMock
//                .Setup(calculator => calculator.GetDiscount(It.IsAny<User>()))
//                .Returns(Task.FromResult(0.0d));

//            var service = new PriceCalculationService(discountCalculatorMock.Object);

//            Action action = () => service.GetRentalCost(null);

//            action.Should().Throw<ArgumentNullException>();
//        }

//        private static object[] _variousPricesParameters = new object[]
//        {
//            new object[]
//            {
//                150f, 0.8f, new DateTime(2019, 4, 11),
//                new DateTime(2019, 4, 13), 20f, 160f
//            },
//            new object[]
//            {
//                150f, 1f, new DateTime(2019, 4, 11),
//                new DateTime(2019, 4, 13), 10f, 170f
//            },
//        };

//        [TestCaseSource(nameof(_variousPricesParameters))]
//        public void VariousPricesTimesAndDistances_ReturnsCorrectResult
//            (float distance, float pricePerKm, DateTime startDate, DateTime endDate,
//            float pricePerDay, float expectedPrice)
//        {
//            Mock<IDiscountCalculator> discountCalculatorMock = new Mock<IDiscountCalculator>();
//            discountCalculatorMock
//                .Setup(calculator => calculator.GetDiscount(It.IsAny<User>()))
//                .Returns(Task.FromResult(0.0d));

//            var service = new PriceCalculationService(discountCalculatorMock.Object);
//            var rental = new Rental
//            {
//                StartDate = startDate,
//                EndDate = endDate,
//                Distance = distance,
//                Car = new Car
//                {
//                    PricePerKM = pricePerKm,
//                    PricePerDay = pricePerDay
//                },
//                User = new User { }
//            };

//            double totalPrice = service.GetRentalCost(rental);

//            totalPrice.Should().BeApproximately(expectedPrice, 0.0001f);
//        }

//        [Test]
//        public void RentalDistanceIs0AndTimeIs0AndPricePerDayIs100AndDiscountIs50Percent_Returns50()
//        {
//            Mock<IDiscountCalculator> discountCalculatorMock = new Mock<IDiscountCalculator>();
//            discountCalculatorMock
//                .Setup(calculator => calculator.GetDiscount(It.IsAny<User>()))
//                .Returns(Task.FromResult(0.5d));

//            var service = new PriceCalculationService(discountCalculatorMock.Object);
//            var rental = new Rental
//            {
//                StartDate = new DateTime(2014, 10, 18),
//                EndDate = new DateTime(2014, 10, 18),
//                Distance = 0f,
//                Car = new Car
//                {
//                    PricePerKM = 10000f,
//                    PricePerDay = 100f
//                }
//            };

//            double totalPrice = service.GetRentalCost(rental);

//            totalPrice.Should().Be(50);
//        }
//    }
//}