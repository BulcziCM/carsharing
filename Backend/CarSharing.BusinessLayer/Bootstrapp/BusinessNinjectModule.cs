﻿using CarSharing.BusinessLayer.Services;
using Ninject.Modules;

namespace CarSharing.BusinessLayer.Bookstrapp
{
    public class BusinessNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IIncomesService>().To<IncomesService>();
            Kernel.Bind<IRentalsService>().To<RentalsService>();
            Kernel.Bind<ICarsService>().To<CarsService>();
            Kernel.Bind<IUsersService>().To<UsersService>();
            Kernel.Bind<ILocationsService>().To<LocationsService>();
            Kernel.Bind<IPriceCalculationService>().To<PriceCalculationService>();
            Kernel.Bind<IDiscountCalculator>().To<DiscountCalculator>();
            Kernel.Bind<ICryptographyService>().To<CryptographyService>();

            Kernel.Bind<IConfigProvider>().To<ConfigProviderAdapter>();

            Kernel.Bind<IDatabaseInitializer>().To<DatabaseInitializer>();
        }
    }
}