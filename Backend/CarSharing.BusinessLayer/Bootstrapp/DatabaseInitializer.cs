﻿using CarSharing.DataLayer;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace CarSharing.BusinessLayer.Bookstrapp
{
    public interface IDatabaseInitializer
    {
        void Initialize();
    }

    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly Func<ICarSharingDbContext> _dbContextFactory;

        public DatabaseInitializer(Func<ICarSharingDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public void Initialize()
        {
            using (var ctx = _dbContextFactory())
            {
                //ctx.Database.EnsureDeleted();
                ctx.Database.EnsureCreated();

                // zaktualizowanie do najnowszej migracji
                //ctx.GetInfrastructure().GetService<IMigrator>().Migrate();
            }
        }
    }
}