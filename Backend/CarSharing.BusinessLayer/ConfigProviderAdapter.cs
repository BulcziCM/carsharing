﻿using ExternalService;

namespace CarSharing.BusinessLayer
{
    public class ConfigProviderAdapter : IConfigProvider
    {
        private UnchangeableConfigProvider _unchangeableConfigProvider = new UnchangeableConfigProvider();

        public double Get(string key)
        {
            return _unchangeableConfigProvider.Get(key);
        }
    }

    public interface IConfigProvider
    {
        double Get(string key);
    }
}