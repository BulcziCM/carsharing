﻿namespace CarSharing.BusinessLayer.Models
{
    public class AvailableCarItem
    {
        public int ID;
        public string Make;
        public string Model;
        public string LicensePlate;
    }
}