﻿namespace CarSharing.BusinessLayer.Models
{
    public class CarIncome
    {
        public int CarId;
        public string Make;
        public string Model;
        public double Income;
    }
}