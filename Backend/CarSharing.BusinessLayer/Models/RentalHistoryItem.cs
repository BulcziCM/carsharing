﻿namespace CarSharing.BusinessLayer.Models
{
    public class RentalHistoryItem
    {
        public string UserName;
        public string UserSurname;
        public string Make;
        public string Model;
        public string LicensePlate;
        public double Cost;
    }
}