﻿namespace CarSharing.BusinessLayer.Models
{
    public class RentalIncomesStatistics
    {
        public double Total;
        public double Average;
        public double Max;
        public double Min;
    }
}