﻿using System.Collections.Generic;

namespace CarSharing.BusinessLayer.Models
{
    public class UserDetails
    {
        public string Name;
        public string Surname;
        public int Age;
        public List<string> Locations;
    }
}