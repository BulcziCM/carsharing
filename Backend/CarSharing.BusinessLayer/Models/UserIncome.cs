﻿namespace CarSharing.BusinessLayer.Models
{
    public class UserIncome
    {
        public int UserId;
        public string Name;
        public string Surname;
        public double Income;
    }
}