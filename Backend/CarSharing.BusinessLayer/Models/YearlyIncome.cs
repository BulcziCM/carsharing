﻿using System;

namespace CarSharing.BusinessLayer.Models
{
    public class YearlyIncome
    {
        public enum Months
        {
            January = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12
        }

        public int Year;
        public MonthlyIncome[] MonthlyIncomes = new MonthlyIncome[12];

        //TAK GENERALNIE NIE ROBIMY!!!
        //Klas zagniezdzonych nie uzywa sie tworzy sie je jedynie na uzytek wewnetrzny klasy,
        //w ktorej sie znajduja (prywatne klasy zagniezdzone).
        public class MonthlyIncome
        {
            public Months Month;
            public double Income;
        }
    }
}