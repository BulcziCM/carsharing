﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarSharing.BusinessLayer.Models;
using CarSharing.DataLayer;
using CarSharing.DataLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace CarSharing.BusinessLayer.Services
{
    public interface ICarsService
    {
        Task<int> AddAsync(Car car);
        Task<Car> GetByIdAsync(int id);
        Task DeleteAsync(int id);
        Task UpdateAsync(Car car);
        Task<List<string>> GetOfferedCarNamesAsync();
        Task<List<AvailableCarItem>> GetAvailableCarsAsync();
        Task<List<AvailableCarItem>> GetAvailableCarsAsync(int page, int pageSize);
        Task<int> GetAllAvailableCarsCount();
    }

    public class CarsService : ICarsService
    {
        private readonly Func<ICarSharingDbContext> _dbContextFactory;

        public CarsService(Func<ICarSharingDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<int> AddAsync(Car car)
        {
            using (var context = _dbContextFactory())
            {
                var entity = context.Cars.Add(car);
                await context.SaveChangesAsync();
                return entity.Entity.Id;
            }
        }

        public async Task<Car> GetByIdAsync(int id)
        {
            using (var context = _dbContextFactory())
            {
                return await context.Cars
                    .FirstOrDefaultAsync(car => car.Id == id);
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (var context = _dbContextFactory())
            {
                var car = await context.Cars.FirstOrDefaultAsync(car => car.Id == id);
                context.Cars.Remove(car);
                await context.SaveChangesAsync();
            }
        }

        public async Task UpdateAsync(Car car)
        {
            using (var context = _dbContextFactory())
            {
                context.Cars.Update(car);
                await context.SaveChangesAsync();
            }
        }

        public async Task<List<AvailableCarItem>> GetAvailableCarsAsync()
        {
            using (var context = _dbContextFactory())
            {
                var availableCars = await context.Cars
                    .Where(car => car.IsAvailable)
                    .Select(car => new AvailableCarItem
                    {
                        ID = car.Id,
                        Make = car.Make,
                        Model = car.Model,
                        LicensePlate = car.LicensePlate
                    })
                    .ToListAsync();

                return availableCars;
            }
        }

        public async Task<List<string>> GetOfferedCarNamesAsync()
        {
            using (var context = _dbContextFactory())
            {
                var offeredCars = await context.Cars
                    .Select(car => $"{car.Make} {car.Model}")
                    .Distinct()
                    .ToListAsync();

                return offeredCars;
            }
        }

        public async Task<List<AvailableCarItem>> GetAvailableCarsAsync(int page, int pageSize)
        {
            using (var context = _dbContextFactory())
            {
                return await context.Cars
                    .Where(x => x.IsAvailable)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .Select(car => new AvailableCarItem
                    {
                        ID = car.Id,
                        Make = car.Make,
                        Model = car.Model,
                        LicensePlate = car.LicensePlate
                    })
                    .ToListAsync();
            }
        }

        public async Task<int> GetAllAvailableCarsCount()
        {
            using (var context = _dbContextFactory())
            {
                return await context.Cars.CountAsync(c => c.IsAvailable);
            }
        }
    }
}