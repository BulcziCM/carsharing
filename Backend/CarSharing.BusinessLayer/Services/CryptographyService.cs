﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace CarSharing.BusinessLayer.Services
{
    public interface ICryptographyService
    {
        string GetMd5Hash(string input);
    }

    public class CryptographyService : ICryptographyService
    {
        //Source: https://docs.microsoft.com/en-us/dotnet/api/system.security.cryptography.md5?view=netframework-4.8
        public string GetMd5Hash(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                StringBuilder sBuilder = new StringBuilder();

                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                return sBuilder.ToString();
            }
        }
    }
}