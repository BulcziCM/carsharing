﻿using CarSharing.DataLayer.Models;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CarSharing.BusinessLayer.Services
{
    internal interface IDiscountCalculator
    {
        Task<double> GetDiscount(User user);
    }

    internal class DiscountCalculator : IDiscountCalculator
    {
        private readonly IConfigProvider _configProvider;
        private readonly ILogger _logger;

        public DiscountCalculator(
            IConfigProvider configProvider,
            ILogger logger)
        {
            _configProvider = configProvider;
            _logger = logger;
        }

        public Task<double> GetDiscount(User user)
        {
            if(user == null)
            {
                _logger.Error("Cant check discounts when user is null");
                throw new ArgumentNullException(nameof(user));
            }

            var task = Task.Factory.StartNew<double>(() =>
            {
                Thread.Sleep(150);
                if (user.IsVip)
                {
                    _logger.Verbose("User is VIP");
                    return _configProvider.Get("VipDiscount");
                }
                else
                {
                    _logger.Verbose("User have no discounts");
                    return 0;
                }
            });

            return task;
        }
    }
}
