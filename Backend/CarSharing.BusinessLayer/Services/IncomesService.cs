﻿using CarSharing.BusinessLayer.Models;
using CarSharing.DataLayer;
using CarSharing.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MonthlyIncome = CarSharing.BusinessLayer.Models.YearlyIncome.MonthlyIncome;

namespace CarSharing.BusinessLayer.Services
{
    public interface IIncomesService
    {
        Task<List<CarIncome>> GetAllCarsIncomeAsync();
        Task<List<UserIncome>> GetAllUsersIncomeAsync();
        Task<List<YearlyIncome>> GetIncomeByYearAsync();
        Task<RentalIncomesStatistics> GetRentalIncomesStatisticsAsync();
        Task GenerateIncomeByYearReportAsync(string outputPath);
    }

    internal class IncomesService : IIncomesService
    {
        private readonly IPriceCalculationService _priceCalculator;
        private readonly Func<ICarSharingDbContext> _dbContextFactory;

        public IncomesService(
            IPriceCalculationService priceCalculator,
            Func<ICarSharingDbContext> dbContextFactory)
        {
            _priceCalculator = priceCalculator;
            _dbContextFactory = dbContextFactory;
        }

        public async Task<List<CarIncome>> GetAllCarsIncomeAsync()
        {
            var incomes = new List<CarIncome>();

            foreach (Rental rental in await GetAllRentalsAsync())
            {
                var cost = _priceCalculator.GetRentalCost(rental);

                CarIncome carIncome = null;

                foreach (var income in incomes)
                {
                    if (income.CarId == rental.Car.Id)
                    {
                        carIncome = income;
                        break;
                    }
                }

                if (carIncome == null)
                {
                    var newCarIncome = new CarIncome
                    {
                        CarId = rental.Car.Id,
                        Make = rental.Car.Make,
                        Model = rental.Car.Model,
                        Income = cost
                    };

                    incomes.Add(newCarIncome);
                }
                else
                {
                    carIncome.Income += cost;
                }
            }

            return incomes;
        }

        private async Task<List<Rental>> GetAllRentalsAsync()
        {
            using (var context = _dbContextFactory())
            {
                return await context.Rentals
                     .Include(r => r.User)
                     .Include(r => r.Car)
                     .ToListAsync();
            }
        }

        public async Task<List<UserIncome>> GetAllUsersIncomeAsync()
        {
            var incomes = new List<UserIncome>();

            foreach (var rental in await GetAllRentalsAsync())
            {
                var cost = _priceCalculator.GetRentalCost(rental);
                var userId = rental.User.Id;

                UserIncome userIncome = null;
                foreach (var income in incomes)
                {
                    if (income.UserId == userId)
                    {
                        userIncome = income;
                        break;
                    }
                }

                if (userIncome == null)
                {
                    var newUserIncome = new UserIncome
                    {
                        UserId = rental.User.Id,
                        Name = rental.User.Name,
                        Surname = rental.User.Surname,
                        Income = cost
                    };

                    incomes.Add(newUserIncome);
                }
                else
                {
                    userIncome.Income += cost;
                }
            }

            return incomes;
        }

        public async Task GenerateIncomeByYearReportAsync(string outputPath)
        {
            var incomeData = await GetIncomeByYearAsync();
            Thread.Sleep(3000);
            var json = JsonConvert.SerializeObject(incomeData);
            File.WriteAllText(outputPath, json);
        }

        public async Task<List<YearlyIncome>> GetIncomeByYearAsync()
        {
            var yearlyIncomes = new List<YearlyIncome>();

            foreach (var rental in await GetAllRentalsAsync())
            {
                var cost = _priceCalculator.GetRentalCost(rental);

                YearlyIncome incomeY = null;

                foreach (var yearlyIncome in yearlyIncomes)
                {
                    if (yearlyIncome.Year == rental.EndDate.Value.Year)
                    {
                        incomeY = yearlyIncome;
                        break;
                    }
                }

                if (incomeY != null)
                {
                    MonthlyIncome monthlyIncome = null;

                    foreach (var income in incomeY.MonthlyIncomes)
                    {
                        if ((int)income.Month == rental.EndDate.Value.Month)
                        {
                            monthlyIncome = income;
                            break;
                        }
                    }

                    if (monthlyIncome == null)
                    {
                        AddMonthlyIncomeToYear(incomeY, rental);
                    }
                    else
                    {
                        monthlyIncome.Income += cost;
                    }
                }
                else
                {
                    var newYearlyIncome = new YearlyIncome
                    {
                        Year = rental.EndDate.Value.Year
                    };

                    AddMonthlyIncomeToYear(newYearlyIncome, rental);
                    yearlyIncomes.Add(newYearlyIncome);
                }
            }

            return yearlyIncomes;
        }

        private void AddMonthlyIncomeToYear(YearlyIncome yearlyIncome, Rental rental)
        {
            var monthNumber = rental.EndDate.Value.Month;

            var newMonthlyIncome = new MonthlyIncome
            {
                Income = _priceCalculator.GetRentalCost(rental),
                Month = (YearlyIncome.Months)monthNumber
            };

            yearlyIncome.MonthlyIncomes[monthNumber - 1] = newMonthlyIncome;
        }

        public async Task<RentalIncomesStatistics> GetRentalIncomesStatisticsAsync()
        {
            List<double> rentalCosts;

            using (var context = _dbContextFactory())
            {
                rentalCosts = await context.Rentals
                    .Include(r => r.Car)
                    .Include(r => r.User)
                    .Select(rental => _priceCalculator.GetRentalCost(rental))
                    .ToListAsync();
            }

            var statistics = new RentalIncomesStatistics
            {
                Average = rentalCosts.Average(/*x => x*/),
                Total = rentalCosts.Sum(),
                Min = rentalCosts.Min(),
                Max = rentalCosts.Max()
            };

            return statistics;
        }
    }
}