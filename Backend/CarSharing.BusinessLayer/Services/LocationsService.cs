﻿using CarSharing.DataLayer.Models;
using CarSharing.DataLayer;
using System;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CarSharing.BusinessLayer.Services
{
    public interface ILocationsService
    {
        Task<int> AddAsync(Location location);
        Task<Location> GetByIdAsync(int id);
    }

    public class LocationsService : ILocationsService
    {
        private readonly Func<ICarSharingDbContext> _dbContextFactory;

        public LocationsService(Func<ICarSharingDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<int> AddAsync(Location location)
        {
            using (var context = _dbContextFactory())
            {
                var entity = context.Locations.Add(location);
                await context.SaveChangesAsync();
                return entity.Entity.Id;
            }
        }

        public async Task<Location> GetByIdAsync(int id)
        {
            using (var context = _dbContextFactory())
            {
                return await context.Locations
                    .FirstOrDefaultAsync(l => l.Id == id);
            }
        }
    }
}