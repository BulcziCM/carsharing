﻿using CarSharing.DataLayer.Models;
using Serilog;
using System;

namespace CarSharing.BusinessLayer.Services
{
    internal interface IPriceCalculationService
    {
        double GetRentalCost(Rental rental);
    }

    internal class PriceCalculationService : IPriceCalculationService
    {
        private IDiscountCalculator _discountCalculator;
        private readonly ILogger _logger;

        public PriceCalculationService(
            IDiscountCalculator discountCalculator,
            ILogger logger)
        {
            _discountCalculator = discountCalculator;
            _logger = logger;
        }

        public double GetRentalCost(Rental rental)
        {
            if (rental == null)
            {
                _logger.Error("Can't calculate rental cost, when rental is null");
                throw new ArgumentNullException(nameof(rental));
            }

            var distanceCost = rental.Distance * rental.Car.PricePerKM;
            var rentalDays = Math.Ceiling((rental.EndDate.Value - rental.StartDate).TotalDays);
            var rentalTimeCost = (rentalDays == 0)
                ? rental.Car.PricePerDay
                : rental.Car.PricePerDay * rentalDays;

            var discountCalculationTask = _discountCalculator.GetDiscount(rental.User);

            double price = distanceCost + rentalTimeCost;
            //do some stuff

            double discount = discountCalculationTask.Result;

            double totalPrice = price * (1 - discount);

            _logger.Verbose($"Calculated cost for rental id = {rental.Id} is {totalPrice}");

            return totalPrice;
        }
    }
}