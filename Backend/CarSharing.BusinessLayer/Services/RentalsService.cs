﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using CarSharing.BusinessLayer.Models;
using CarSharing.DataLayer;
using CarSharing.DataLayer.Models;
using System.Threading.Tasks;

namespace CarSharing.BusinessLayer.Services
{
    public interface IRentalsService
    {
        Task<int> StartRentalAsync(int userID, int carID, DateTime startDate);
        Task EndRentalAsync(int rentalID, double distance, DateTime endDate);
        Task<List<RentalHistoryItem>> GetFullRentalHistoryAsync();
        Task<List<RentalHistoryItem>> GetRentalHistoryByUserIdAsync(int userID);
    }

    internal class RentalsService : IRentalsService
    {
        private readonly IUsersService _userService;
        private readonly ICarsService _carService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly Func<ICarSharingDbContext> _dbContextFactory;

        public RentalsService(
            IUsersService userService,
            ICarsService carService,
            IPriceCalculationService priceCalculationService,
            Func<ICarSharingDbContext> dbContextFactory)
        {
            _userService = userService;
            _carService = carService;
            _priceCalculationService = priceCalculationService;
            _dbContextFactory = dbContextFactory;
        }

        public async Task<int> StartRentalAsync(int userID, int carID, DateTime startDate)
        {
            var choosenUser = await _userService.GetByIdAsync(userID);

            if (choosenUser == null)
            {
                throw new Exception($"There is no user with ID {userID}");
            }

            Car choosenCar = await _carService.GetByIdAsync(carID);

            if (choosenCar == null)
            {
                throw new Exception($"Car with ID {carID} not found");
            }

            if (!choosenCar.IsAvailable)
            {
                throw new Exception($"Car with ID {carID} is not available");
            }

            var newRental = new Rental
            {
                CarId = choosenCar.Id,
                UserId = choosenUser.Id,
                Distance = 0.0d,
                IsActive = true,
                StartDate = startDate
            };

            choosenCar.IsAvailable = false;

            using (var context = _dbContextFactory())
            {
                var rentalEntity = context.Rentals.Add(newRental);
                await context.SaveChangesAsync();
                return rentalEntity.Entity.Id;
            }
        }

        public async Task EndRentalAsync(int rentalID, double distance, DateTime endDate)
        {
            using (var context = _dbContextFactory())
            {
                var choosenRental = await context.Rentals
                    .Include(r => r.Car)
                    .SingleOrDefaultAsync(r => r.Id == rentalID);

                if (choosenRental == null)
                {
                    throw new Exception($"Rental with ID {rentalID} not found");
                }

                if (!choosenRental.IsActive)
                {
                    throw new Exception($"Rental with ID {rentalID} is not active");
                }

                if (endDate < choosenRental.StartDate)
                {
                    throw new Exception($"Rental cannot end ({endDate}) before it starts ({choosenRental.StartDate})");
                }

                choosenRental.Car.IsAvailable = true;
                choosenRental.Distance = distance;
                choosenRental.IsActive = false;
                choosenRental.EndDate = endDate;

                await context.SaveChangesAsync();
            }
        }

        public async Task<List<RentalHistoryItem>> GetFullRentalHistoryAsync()
        {
            using (var context = _dbContextFactory())
            {
                return await GetValidRentalHistoryItemsAsync(context.Rentals.AsQueryable());
            }
        }

        public async Task<List<RentalHistoryItem>> GetRentalHistoryByUserIdAsync(int userID)
        {
            using (var context = _dbContextFactory())
            {
                if (!context.Users.Any(user => user.Id == userID))
                {
                    throw new Exception($"There is no user with id {userID}");
                }

                var userRentalsSubQuery = context.Rentals
                    .Where(rental => rental.User.Id == userID);

                return await GetValidRentalHistoryItemsAsync(userRentalsSubQuery);
            }
        }

        private async Task<List<RentalHistoryItem>> GetValidRentalHistoryItemsAsync(IQueryable<Rental> rentalQueryBase)
        {
            var rentalHistory = (await rentalQueryBase
                .Include(r => r.Car)
                .Include(r => r.User)
                .Where(rental => !rental.IsActive)
                .Select(rental => new RentalHistoryItem
                {
                    Make = rental.Car.Make,
                    Model = rental.Car.Model,
                    LicensePlate = rental.Car.LicensePlate,
                    UserName = rental.User.Name,
                    UserSurname = rental.User.Surname,
                    Cost = _priceCalculationService.GetRentalCost(rental)
                })
                .ToListAsync())
                .OrderByDescending(rentalItem => rentalItem.Cost)
                .ToList();

            return rentalHistory;
        }
    }
}