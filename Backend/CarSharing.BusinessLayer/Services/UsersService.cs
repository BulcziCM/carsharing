﻿using CarSharing.BusinessLayer.Models;
using CarSharing.DataLayer;
using CarSharing.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;

namespace CarSharing.BusinessLayer.Services
{
    public interface IUsersService
    {
        Task<int> AddUserAsync(User user, int locationId);
        Task<User> GetByIdAsync(int id);
        Task<UserDetails> GetUserDetalsAsync(int userID);
        Task<bool> AuthenticateUser(string login, string passwordHash);
    }

    public class UsersService : IUsersService
    {
        private readonly Func<ICarSharingDbContext> _dbContextFactory;

        public UsersService(Func<ICarSharingDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<int> AddUserAsync(User user, int locationId)
        {
            using (var context = _dbContextFactory())
            {
                var newUserEntity = context.Users.Add(user);
                
                await context.SaveChangesAsync();
                
                var newUserId = newUserEntity.Entity.Id;

                var newUserLocation = new UsersLocations
                {
                    LocationId = locationId,
                    UserId = newUserId
                };

                context.UsersLocations.Add(newUserLocation);
                await context.SaveChangesAsync();

                return newUserId;
            }
        }

        public async Task<bool> AuthenticateUser(string login, string passwordHash)
        {
            using (var context = _dbContextFactory())
            {
                return await context.Users.AnyAsync(x =>
                    x.Login.ToLower() == login.ToLower()
                    && x.PasswordHash.ToLower() == passwordHash.ToLower());
            }
        }

        public async Task<User> GetByIdAsync(int id)
        {
            using (var context = _dbContextFactory())
            {
                return await context.Users
                    .FirstOrDefaultAsync(car => car.Id == id);
            }
        }

        public async Task<UserDetails> GetUserDetalsAsync(int userID)
        {
            User user;
            List<string> locationNames;

            using (var context = _dbContextFactory())
            {
                var subQuery = context.UsersLocations
                    .Include(l => l.User)
                    .Include(l => l.Location)
                    .Where(l => l.UserId == userID);

                user = subQuery.Select(ul => ul.User).First();
                locationNames = await subQuery.Select(ul => ul.Location.Name).ToListAsync();
            }

            var result = new UserDetails
            {
                Name = user.Name,
                Surname = user.Surname,
                Age = user.Age,
                Locations = locationNames
            };

            return result;
        }
    }
}