﻿using Ninject.Modules;

namespace CarSharing.DataLayer.Bootstrapp
{
    public class DataLayerNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ICarSharingDbContext>().ToMethod(x => new CarSharingDbContext());
        }
    }
}