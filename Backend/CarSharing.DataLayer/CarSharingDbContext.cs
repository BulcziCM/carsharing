﻿using CarSharing.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CarSharing.DataLayer
{
    public interface ICarSharingDbContext : IDisposable
    {
        DbSet<Car> Cars { get; set; }
        DbSet<Location> Locations { get; set; }
        DbSet<Rental> Rentals { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<UsersLocations> UsersLocations { get; set; }

        DatabaseFacade Database { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }

    public class CarSharingDbContext : DbContext, ICarSharingDbContext
    {
        private const string _connectionString = "Data Source=.;Initial Catalog=CarSharing;Integrated Security=True;";

        public DbSet<User> Users { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Rental> Rentals { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<UsersLocations> UsersLocations { get; set; }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}