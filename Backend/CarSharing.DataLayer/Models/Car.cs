﻿namespace CarSharing.DataLayer.Models
{
    public class Car : Entity
    {
        public string Make{ get; set; }
        public string Model{ get; set; }
        public string LicensePlate{ get; set; }
        public bool IsAvailable{ get; set; }
        public int SeatsCount{ get; set; }
        public double PricePerKM{ get; set; }
        public double PricePerDay{ get; set; }
        public int RegionId { get; set; }
    }
}