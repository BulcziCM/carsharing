﻿using System.Collections.Generic;

namespace CarSharing.DataLayer.Models
{
    public class Location : Entity
    {
        public string Name { get; set; }
        public List<UsersLocations> Users { get; set; } = new List<UsersLocations>();
    }
}