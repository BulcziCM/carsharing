﻿using System;

namespace CarSharing.DataLayer.Models
{
    public class Rental : Entity
    {
        public double Distance { get; set; }
        public int CarId { get; set; }
        public Car Car { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public bool IsActive { get; set; }
        public DateTime  StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}