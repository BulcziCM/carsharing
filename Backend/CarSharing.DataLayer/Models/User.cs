﻿using System.Collections.Generic;

namespace CarSharing.DataLayer.Models
{
    public class User : Entity
    {
        public string Login { get; set; }
        public string PasswordHash { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public string DrivingLicenseNumber { get; set; }
        public List<UsersLocations> Locations { get; set; } = new List<UsersLocations>();
        public bool IsVip { get; set; }
    }
}