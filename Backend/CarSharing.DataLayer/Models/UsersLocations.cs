﻿namespace CarSharing.DataLayer.Models
{
    public class UsersLocations : Entity
    {
        public User User { get; set; }
        public int UserId { get; set; }
        public Location Location { get; set; }
        public int LocationId { get; set; }
    }
}