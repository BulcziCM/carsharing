﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.IO;

namespace CarSharing.WebApi
{
    internal class AppHost
    {
        private IWebHost _webHost;

        //Swagger reference: https://docs.microsoft.com/en-us/aspnet/core/tutorials/web-api-help-pages-using-swagger?view=aspnetcore-3.1
        //CORS reference: https://stackoverflow.com/questions/31942037/how-to-enable-cors-in-asp-net-core
        public void Start()
        {
            _webHost = WebHost.CreateDefaultBuilder()
               .ConfigureServices(services =>
               {
                   services.AddMvc();
                   services.AddCors(options =>
                   {
                       options.AddPolicy("AllowAllOrigins",
                       builder =>
                       {
                           //To allows all sources
                           builder
                            .AllowAnyOrigin()
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials();
                       });
                   });
                   services.AddSwaggerGen(SwaggerDocsConfig);
               })
               .Configure(app =>
               {
                   app.UseCors("AllowAllOrigins"); //To use policy created above. Make sure UseCors is before UseMvc.
                   app.UseMvc();
                   app.UseSwagger();
                   app.UseSwaggerUI(c =>
                   {
                       c.SwaggerEndpoint("/swagger/v1/swagger.json", TextConstans.ServiceName);
                       c.RoutePrefix = string.Empty;
                   });
               })
               .UseSerilog()
               .UseUrls("http://*:10500")
               .Build();

            _webHost.Start();
        }

        private void SwaggerDocsConfig(SwaggerGenOptions genOptions)
        {
            genOptions.SwaggerDoc(
                "v1",
                new Info
                {
                    Version = "v1",
                    Title = TextConstans.ServiceName,
                    Description = TextConstans.ServiceDescription,
                    TermsOfService = "https://webapiexamples.project.com/terms",
                    Contact = new Contact
                    {
                        Name = "Jakub Bulczak",
                        Email = "kuba@codementors.pl",
                        Url = "https://www.linkedin.com/in/jakub-bulczak-21873064/"
                    },
                    License = new License
                    {
                        Name = "Use some license",
                        Url = "https://webapiexamples.project.com/license"
                    }
                });

            //Descriptions from summaries requires checking up a checkbox: RightClick on project -> Properties -> Build -> XML Documentation file . Leave the default path value.
            var xmlFile = Path.ChangeExtension(typeof(Program).Assembly.Location, ".xml");
            genOptions.IncludeXmlComments(xmlFile);
        }

        public void Stop()
        {
            _webHost.StopAsync().Wait();
            _webHost.Dispose();
        }
    }
}