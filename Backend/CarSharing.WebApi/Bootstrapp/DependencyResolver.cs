﻿using CarSharing.BusinessLayer.Bookstrapp;
using CarSharing.DataLayer.Bootstrapp;
using Ninject;
using Ninject.Modules;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Json;

namespace CarSharing.WebApi.Bootstrapp
{
    public static class DependencyResolver
    {
        private static IKernel _kernel = null;

        public static IKernel GetKernel()
        {
            if(_kernel != null)
            {
                return _kernel;
            }

            _kernel = new StandardKernel();

            _kernel.Bind<ILogger>().ToConstant(SetUpLogger());
            _kernel.Load(new INinjectModule[]
            {
                new BusinessNinjectModule(),
                new DataLayerNinjectModule()
            });

            return _kernel;
        }

        private static ILogger SetUpLogger()
        {
            var logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.File(
                    new JsonFormatter(),
                    "./logs/CarSharing_WebAPI.txt",
                    rollingInterval: RollingInterval.Day,
                    restrictedToMinimumLevel: LogEventLevel.Information)
                .MinimumLevel.Verbose()
                .CreateLogger();

            return logger;
        }
    }
}