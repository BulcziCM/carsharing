﻿namespace CarSharing.WebApi
{
    internal class TextConstans
    {
        public static string ServiceName => "Codementors CarSharing WebApi Service";
        public static string ServiceDescription => "A ASP.NET Core Web API for sharing vehicles";
    }
}