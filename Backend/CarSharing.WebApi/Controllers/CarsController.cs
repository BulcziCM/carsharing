﻿using CarSharing.BusinessLayer.Models;
using CarSharing.BusinessLayer.Services;
using CarSharing.DataLayer.Models;
using CarSharing.WebApi.Bootstrapp;
using CarSharing.WebApi.Models;
using Castle.Core.Logging;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Ninject;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarSharing.WebApi.Controllers
{
    [Route("api/cars")]
    public class CarsController
    {
        private readonly ICarsService _carsService;
        private readonly ILogger _logger;

        public CarsController()
        {
            var kernel = DependencyResolver.GetKernel();

            _carsService = kernel.Get<ICarsService>();
            //_logger = kernel.Get<ILogger>();
        }

        /*
        Method: GET
        URI: http://localhost:10500/api/cars/1
        Body: no body
        */
        [HttpGet("{id}")]
        public async Task<Car> GetCarAsync(int id)
        {
            //_logger.Debug($"CarsController Get with id = {id} called");

            var result = await _carsService.GetByIdAsync(id);

            //_logger.Debug($"Object returned from call: {JsonConvert.SerializeObject(result)}");

            return result;
        }

        /*
        Method: POST
        URI: http://localhost:10500/api/cars
        Body:
        {
            "id": 0,
            "make": "VW",
            "model": "Passat",
            "licensePlate": "GD12345",
            "isAvailable": true,
            "seatsCount": 5,
            "pricePerKM": 1.5,
            "pricePerDay": 120,
            "regionId": 1
        }
        */
        [HttpPost]
        public async Task<int> PostCarAsync([FromBody]Car car)
        {
            var result = await _carsService.AddAsync(car);
            return result;
        }

        /*
        Method: PUT
        URI: http://localhost:10500/api/cars/1
        Body:
        {
            "id": 1,
            "make": "VW",
            "model": "Passat",
            "licensePlate": "GD12345",
            "isAvailable": true,
            "seatsCount": 5,
            "pricePerKM": 1.5,
            "pricePerDay": 120,
            "regionId": 1
        }
        */
        [HttpPut("{id}")]
        public async Task UpdateCarAsync(int id, [FromBody]Car car)
        {
            car.Id = id;
            await _carsService.UpdateAsync(car);
        }

        /*
        Method: DELETE
        URI: http://localhost:10500/api/cars/1
        Body: no body
        */
        [HttpDelete("{id}")]
        public async Task DeleteCarAsync(int id)
        {
            await _carsService.DeleteAsync(id);
        }

        /*
        Method: GET
        URI: http://localhost:10500/api/cars/available
        Body: no body
        */
        [HttpGet("available")]
        public async Task<List<AvailableCarItem>> GetAvailableCarsAsync()
        {
            var result = await _carsService.GetAvailableCarsAsync();
            return result;
        }

        /*
        Method: GET
        URI: http://localhost:10500/api/cars/available/page/1
        Body: no body
        */
        [HttpGet("available/page/{page}/{pageSize}")]
        public async Task<PagedResults<AvailableCarItem>> GetAvailableCarsPagedAsync(int page, int pageSize)
        {
            var carsCount = await _carsService.GetAllAvailableCarsCount();
            var carItems  = await _carsService.GetAvailableCarsAsync(page, pageSize);

            var minPageNumber = 1;
            var maxPageNumber = carsCount == 0 ? 1 : (int)Math.Ceiling((double)carsCount / (double)pageSize);

            var response = new PagedResults<AvailableCarItem>
            {
                Page = page,
                PageSize = pageSize,
                ResultsCount = carsCount,
                PagesCount = maxPageNumber,
                NextPageAttributes = page < maxPageNumber ? $"/{page + 1}/{pageSize}" : null,
                PrevPageAttributes = page > minPageNumber ? $"/{page - 1}/{pageSize}" : null,
                Results = carItems
            };

            return response;
        }

        /*
        Method: GET
        URI: http://localhost:10500/api/cars/names
        Body: no body
        */
        [HttpGet("names")]
        public async Task<List<string>> GetCarsNamesAsync()
        {
            var result = await _carsService.GetOfferedCarNamesAsync();
            return result;
        }
    }
}