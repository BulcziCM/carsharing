﻿using CarSharing.BusinessLayer.Models;
using CarSharing.BusinessLayer.Services;
using CarSharing.WebApi.Bootstrapp;
using Microsoft.AspNetCore.Mvc;
using Ninject;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarSharing.WebApi.Controllers
{
    [Route("api/incomes")]
    public class IncomesController : ControllerBase
    {
        private readonly IIncomesService _incomeService;

        public IncomesController()
        {
            var kernel = DependencyResolver.GetKernel();

            _incomeService = kernel.Get<IIncomesService>();
        }

        [HttpGet("cars")]
        public async Task<List<CarIncome>> GetCarsIncomes()
        {
            var result = await _incomeService.GetAllCarsIncomeAsync();
            return result;
        }

        [HttpGet("users")]
        public async Task<List<UserIncome>> GetUsersIncomes()
        {
            var result = await _incomeService.GetAllUsersIncomeAsync();
            return result;
        }

        [HttpGet("yearly")]
        public async Task<List<YearlyIncome>> GetYeatlyIncomes()
        {
            var result = await _incomeService.GetIncomeByYearAsync();
            return result;
        }

        [HttpGet("statistics")]
        public async Task<RentalIncomesStatistics> GetIncomeStatistics()
        {
            var result = await _incomeService.GetRentalIncomesStatisticsAsync();
            return result;
        }
    }
}