﻿using CarSharing.BusinessLayer.Services;
using CarSharing.DataLayer.Models;
using CarSharing.WebApi.Bootstrapp;
using Microsoft.AspNetCore.Mvc;
using Ninject;
using System.Threading.Tasks;

namespace CarSharing.WebApi.Controllers
{
    [Route("api/locations")]
    public class LocationsController : ControllerBase
    {
        private ILocationsService _locationsService;

        public LocationsController()
        {
            var kernel = DependencyResolver.GetKernel();

            _locationsService = kernel.Get<ILocationsService>();
        }

        [HttpGet("{id}")]
        public async Task<Location> GetLocationAsync(int id)
        {
            var location = await _locationsService.GetByIdAsync(id);
            return location;
        }

        [HttpPost]
        public async Task<int> PostLocationAsync([FromBody] Location location)
        {
            var locationId = await _locationsService.AddAsync(location);
            return locationId;
        }
    }
}