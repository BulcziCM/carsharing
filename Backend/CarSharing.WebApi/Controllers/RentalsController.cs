﻿using CarSharing.BusinessLayer.Models;
using CarSharing.BusinessLayer.Services;
using CarSharing.WebApi.Bootstrapp;
using CarSharing.WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Ninject;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarSharing.WebApi.Controllers
{
    [Route("api/rentals")]
    public class RentalsController : ControllerBase
    {
        private readonly IRentalsService _rentalsService;
        public RentalsController()
        {
            var kernel = DependencyResolver.GetKernel();

            _rentalsService = kernel.Get<IRentalsService>();
        }

        [HttpPost]
        public async Task<int> PostStartRentalAsync([FromBody]StartRentalData startRentalData)
        {
            var rentalID = await _rentalsService.StartRentalAsync(
                startRentalData.UserID,
                startRentalData.CarID,
                startRentalData.StartDate);

            return rentalID;
        }

        [HttpPut("{id}")]
        public async Task PutEndRentalAsync([FromBody]EndRentalData endRentalData, int id)
        {
            await _rentalsService.EndRentalAsync(
                id,
                endRentalData.Distance,
                endRentalData.EndDate);
        }

        [HttpGet("history/full")]
        public async Task<List<RentalHistoryItem>> GetFullHistoryAsync()
        {
            var result = await _rentalsService.GetFullRentalHistoryAsync();
            return result;
        }

        [HttpGet("history/user/{id}")]
        public async Task<List<RentalHistoryItem>> GetHistoryForUserAsync(int id)
        {
            var result = await _rentalsService.GetRentalHistoryByUserIdAsync(id);
            return result;
        }
    }
}