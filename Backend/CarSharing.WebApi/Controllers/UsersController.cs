﻿using CarSharing.BusinessLayer.Models;
using CarSharing.BusinessLayer.Services;
using CarSharing.DataLayer.Models;
using CarSharing.WebApi.Bootstrapp;
using CarSharing.WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Ninject;
using System.Threading.Tasks;

namespace CarSharing.WebApi.Controllers
{
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private IUsersService _usersService;

        public UsersController()
        {
            var kernel = DependencyResolver.GetKernel();

            _usersService = kernel.Get<IUsersService>();
        }

        [HttpPost("auth")]
        public async Task<bool> PostAuthenticateUser([FromBody] UserAuthenticationData userData)
        {
            return await _usersService.AuthenticateUser(userData.UserName, userData.Password);
        }

        [HttpPost]
        public async Task<int> PostUser([FromBody]NewUserData newUser)
        {
            var userId = await _usersService.AddUserAsync(newUser.User, newUser.LocationId);
            return userId;
        }

        [HttpGet("{id}")]
        public async Task<User> GetUser(int id)
        {
            var user = await _usersService.GetByIdAsync(id);
            return user;
        }

        [HttpGet("{id}/details")]
        public async Task<UserDetails> GetAllUsers(int id)
        {
            var userDetails = await _usersService.GetUserDetalsAsync(id);
            return userDetails;
        }
    }
}