﻿using System;

namespace CarSharing.WebApi.Models
{
    public class EndRentalData
    {
        public double Distance { get; set; }
        public DateTime EndDate { get; set; }
    }
}