﻿using CarSharing.DataLayer.Models;

namespace CarSharing.WebApi.Models
{
    public class NewUserData
    {
        public User User { get; set; }
        public int LocationId { get; set; }
    }
}