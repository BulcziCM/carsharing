﻿using System;

namespace CarSharing.WebApi.Models
{
    public class StartRentalData
    {
        public int UserID { get; set; }
        public int CarID { get; set; }
        public DateTime StartDate { get; set; }
    }
}