﻿using CarSharing.WebApi.Bootstrapp;
using Microsoft.AspNetCore.Hosting;
using Ninject;
using Serilog;
using System;
using Topshelf;

namespace CarSharing.WebApi
{
    internal class Program
    {
        static void Main()
        {
            var logger = DependencyResolver.GetKernel()
                .Get<ILogger>();

            logger.Information("Starting WebApi");

            var host = HostFactory.Run(config =>
            {
                config.Service<AppHost>(s =>
                {
                    s.ConstructUsing(_ => new AppHost());
                    s.WhenStarted(appHost => appHost.Start());
                    s.WhenStopped(appHost => appHost.Stop());
                });

                config.RunAsLocalSystem();

                config.SetServiceName(TextConstans.ServiceName + "1");
                config.SetDisplayName(TextConstans.ServiceName + "1");
                config.SetDescription(TextConstans.ServiceDescription);
            });

            var exitCode = (int)Convert.ChangeType(host, host.GetTypeCode());
            Environment.ExitCode = exitCode;

            logger.Information($"Closing WebApi. ExitCode: {exitCode}");
        }
    }
}