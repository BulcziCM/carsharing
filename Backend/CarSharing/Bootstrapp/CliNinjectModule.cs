﻿using CarSharing.Cli.Helpers;
using Ninject.Modules;

namespace CarSharing.Cli.Bootstrapp
{
    public class CliNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IProgram>().To<Program>();
            Kernel.Bind<IIoHelper>().To<IoHelper>();
            Kernel.Bind<IMenu>().To<Menu>();
        }
    }
}