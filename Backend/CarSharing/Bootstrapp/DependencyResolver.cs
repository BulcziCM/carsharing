﻿using CarSharing.BusinessLayer.Bookstrapp;
using CarSharing.DataLayer.Bootstrapp;
using Ninject;
using Ninject.Modules;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Json;

namespace CarSharing.Cli.Bootstrapp
{
    public static class DependencyResolver
    {
        public static IKernel GetKernel()
        {
            var kernel = new StandardKernel();

            kernel.Bind<ILogger>().ToConstant(SetUpLogger());

            kernel.Load(new INinjectModule[]
            {
                new CliNinjectModule(),
                new BusinessNinjectModule(),
                new DataLayerNinjectModule()
            });

            return kernel;
        }

        private static ILogger SetUpLogger()
        {
            var logger = new LoggerConfiguration()
                .WriteTo.File(
                    new JsonFormatter(),
                    "./logs/CarSharing_CLI.txt",
                    rollingInterval: RollingInterval.Day,
                    restrictedToMinimumLevel: LogEventLevel.Verbose)
                .MinimumLevel.Verbose()
                .CreateLogger();

            return logger;
        }
    }
}