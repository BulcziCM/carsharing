﻿using CarSharing.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CarSharing.Cli.Helpers
{
    internal interface IIoHelper
    {
        bool GetBoolFromUser(string message);
        DateTime GetDateFromUser(string message);
        double GetDoubleFromUser(string message);
        int GetIntFromUser(string message);
        string GetStringFromUser(string message);
        void PrintRentalHistoryItems(List<RentalHistoryItem> rentalHistory);
    }

    internal class IoHelper : IIoHelper
    {
        //Uwspolnic kod metod pobiwerania danych od uzytkownika
        //wykorzystujac Func i Funkcje generyczne, np GetDataFromUser<T>(input, Func<T, string (input)>

        public DateTime GetDateFromUser(string message)
        {
            const string dateFormat = "dd-MM-yyyy";
            var result = new DateTime();
            var success = false;

            while (!success)
            {
                Console.Write($"{message} ({dateFormat}): ");
                var input = Console.ReadLine();
                success = DateTime.TryParseExact(
                    input,
                    dateFormat,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out result);

                if (!success)
                {
                    Console.WriteLine("It was not a date in a correct format. Try again...");
                }
            }

            return result;
        }

        public int GetIntFromUser(string message)
        {
            int result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = int.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It was not a number. Try again...");
                }
            }

            return result;
        }

        public bool GetBoolFromUser(string message)
        {
            bool result = false;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = bool.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It was not a true or false. Try again...");
                }
            }

            return result;
        }

        public double GetDoubleFromUser(string message)
        {
            double result = 0;
            var success = false;

            while (!success)
            {
                Console.Write($"{message}: ");
                var input = Console.ReadLine();
                success = double.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It was not a number");
                }
            }

            return result;
        }

        public string GetStringFromUser(string message)
        {
            Console.Write($"{message}: ");
            return Console.ReadLine();
        }

        public void PrintRentalHistoryItems(List<RentalHistoryItem> rentalHistory)
        {
            if (!rentalHistory.Any())
            {
                Console.WriteLine("No items to show");
                return;
            }

            foreach (var historyItem in rentalHistory)
            {
                Console.WriteLine($"{historyItem.UserName} {historyItem.UserSurname} {historyItem.LicensePlate} {historyItem.Make} {historyItem.Model} {historyItem.Cost}");
            }
        }
    }
}