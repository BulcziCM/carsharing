﻿using System;
using System.Collections.Generic;

namespace CarSharing.Cli.Helpers
{
    internal interface IMenu
    {
        void AddMenuItem(Action action, string description, int key);
        void ExecuteAction(int key);
        void ShowMenu();
    }

    internal class Menu : IMenu
    {
        private Dictionary<int, MenuItem> _menuItems = new Dictionary<int, MenuItem>();

        public void AddMenuItem(Action action, string description, int key)
        {
            if (_menuItems.ContainsKey(key))
            {
                throw new Exception($"There is already a menu item with key {key}");
            }

            _menuItems.Add(
                key,
                new MenuItem
                {
                    Action = action,
                    Description = description
                });
        }

        public void ExecuteAction(int key)
        {
            if (!_menuItems.ContainsKey(key))
            {
                throw new Exception($"There is no action for key {key} defined");
            }

            _menuItems[key].Action();
        }

        public void ShowMenu()
        {
            Console.WriteLine("Available options:");
            foreach (var item in _menuItems)
            {
                Console.WriteLine($"\t{item.Key} {item.Value.Description}");
            }
        }
    }
}