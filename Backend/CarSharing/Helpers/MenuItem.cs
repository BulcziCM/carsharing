﻿using System;

namespace CarSharing.Cli.Helpers
{
    internal class MenuItem
    {
        public string Description;
        public Action Action;
    }
}