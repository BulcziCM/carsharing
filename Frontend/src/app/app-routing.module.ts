import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AvailableCarsListComponent } from './available-cars-list/available-cars-list.component';
import { HomeComponent } from './home/home.component';
import { CarDetailsComponent } from './car-details/car-details.component';
import { CarUpdateComponent } from './car-update/car-update.component';
import { CarAddComponent } from './car-add/car-add.component';
import { IsUserLoggedGuard } from './security/is-user-logged.guard';
import { LogInComponent } from './log-in/log-in.component';

const routes: Routes = [
  { path: '',      redirectTo: 'home',           pathMatch: 'full' },
  { path: 'auth',  component: LogInComponent,    pathMatch: 'full' },
  { path: 'home',  component: HomeComponent,     pathMatch: 'full' },
  { path: 'cars',  children: [
    { path: 'add', component: CarAddComponent,   pathMatch: 'full', canActivate: [IsUserLoggedGuard] },
    { path: ':id', children: [
      { path: 'update', component: CarUpdateComponent,  pathMatch: 'full' },
      { path: '',       component: CarDetailsComponent, pathMatch: 'full' },
    ] },
    { path: '', component: AvailableCarsListComponent,  pathMatch: 'full' },
  ] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
