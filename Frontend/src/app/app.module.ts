import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { StorageServiceModule } from 'angular-webstorage-service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AvailableCarsListComponent } from './available-cars-list/available-cars-list.component';
import { HomeComponent } from './home/home.component';
import { CarDetailsComponent } from './car-details/car-details.component';
import { CarUpdateComponent } from './car-update/car-update.component';
import { CarAddComponent } from './car-add/car-add.component';
import { ErrorsListComponent } from './errors-list/errors-list.component';
import { IsUserLoggedGuard } from './security/is-user-logged.guard';
import { LogInComponent } from './log-in/log-in.component';
import { LocalStorageService } from './services/local-storage/local-storage.service';

@NgModule({
  declarations: [
    AppComponent,
    AvailableCarsListComponent,
    HomeComponent,
    CarDetailsComponent,
    CarUpdateComponent,
    CarAddComponent,
    ErrorsListComponent,
    LogInComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    StorageServiceModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    LocalStorageService,
    IsUserLoggedGuard
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }
