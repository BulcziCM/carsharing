import { Component, OnInit } from '@angular/core';
import { CarItem } from '../models/car-item';
import { CarsService } from '../services/cars-service/cars.service'
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PagedCarResults } from '../models/paged-car-results';

@Component({
  selector: 'app-available-cars-list',
  templateUrl: './available-cars-list.component.html',
  styleUrls: ['./available-cars-list.component.scss']
})

export class AvailableCarsListComponent implements OnInit {

  carsPageData : PagedCarResults = undefined;
  carDeleteModalDataContex : CarItem = new CarItem();

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private carsService : CarsService) { }

  ngOnInit() {
    this.getCarsPage(1, 2);
  }

  getCarsPage(page : number, pageSize : number) : void {
    this.getCarsPageByAttributes('/' + page + '/' + pageSize);
  }

  getCarsPageByAttributes(attributes : string) : void {
    this.carsService.getCarsPage(attributes)
      .subscribe(
        (items) => {
          this.carsPageData = items;
        },
        () => {
          this.carsPageData = undefined;
        }
      );
  }

  showCarDetails(carId : number) : void {
    this.router.navigate(['/cars/' + carId]);
  }

  updateCar(carId: number) : void {
    this.router.navigate(['/cars/' + carId + '/update']);
  }

  addCar() : void {
    this.router.navigate(['cars/add']);
  }

  setModalDataContext(car : CarItem) : void {
    this.carDeleteModalDataContex = car;
  }

  modalCanceled() : void {
    this.carDeleteModalDataContex = new CarItem();
  }

  deleteCar(carId : number) : void {
    this.carsService.delete(carId)
      .subscribe(
        () => {
          this.getCarsPage(this.carsPageData.page, 2);
          this.toastr.success('Car ID: ' + carId + ' deleted successfully', 'Success');
        },
        (err) => {
          this.toastr.error('Error when deleting a car', 'Error');
        }
      );
  }
}
