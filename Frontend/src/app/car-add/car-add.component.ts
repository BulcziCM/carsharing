import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { CarsService } from '../services/cars-service/cars.service';
import { CarItemValidatorService } from '../services/car-item-validator/car-item-validator.service';
import { Car } from '../models/car';

@Component({
  selector: 'app-car-add',
  templateUrl: './car-add.component.html',
  styleUrls: ['./car-add.component.scss']
})

export class CarAddComponent implements OnInit {

  errors : string[] = [];
  newCar : Car;

  constructor(
    private router : Router,
    private toastr: ToastrService,
    private carItemValidator : CarItemValidatorService,
    private carsService : CarsService)
    { }

  ngOnInit() {
    this.newCar = new Car();
    this.newCar.isAvailable = true;
  }

  addCarToList() : void {
    this.errors = this.carItemValidator.isCarItemValid(this.newCar);

    if(this.errors.length > 0)
    {
      return;
    }

    this.carsService.add(this.newCar)
      .subscribe(
        (carId) => {
          this.toastr.success('Car added successfully. Id = ' + carId, 'Add car result');
          this.router.navigate(['/cars/' + carId])
        },
        () => {
          this.toastr.error('An error occured when adding new car', 'Add car result');
        }
      )
  }
}
