import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { CarsService } from '../services/cars-service/cars.service';

import { Car } from '../models/car';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.scss']
})
export class CarDetailsComponent implements OnInit {

  selectedCar : Car;

  constructor(
    private router: Router,
    private route : ActivatedRoute,
    private carsService : CarsService) { }

  ngOnInit() {
    let carId = Number(this.route.snapshot.paramMap.get('id'));
    this.carsService.get(carId)
      .subscribe(
        (car) => {
          this.selectedCar = car
        },
        () => { }
      );
  }

  updateCar() : void {
    this.router.navigate(['/cars/' + this.selectedCar.id + '/update']);
  }

  goBack() : void {
    this.router.navigate(['/cars']);
  }
}
