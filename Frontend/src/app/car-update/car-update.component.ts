import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

import { CarsService } from '../services/cars-service/cars.service';
import { CarItemValidatorService } from '../services/car-item-validator/car-item-validator.service';
import { Car } from '../models/car';

@Component({
  selector: 'app-car-update',
  templateUrl: './car-update.component.html',
  styleUrls: ['./car-update.component.scss']
})
export class CarUpdateComponent implements OnInit {

  errors : string[] = [];
  carToUpdate : Car;

  constructor(
    private router: Router,
    private route : ActivatedRoute,
    private location: Location,
    private toastr: ToastrService,
    private carItemValidator: CarItemValidatorService,
    private carsService : CarsService) { }

  ngOnInit() {
    let carId = Number(this.route.snapshot.paramMap.get('id'));
    this.carsService.get(carId)
      .subscribe(
        (car) => this.carToUpdate = car,
        (err) => this.toastr.error(err)
      );
  }

  saveUpdate() : void {
    this.errors = this.carItemValidator.isCarItemValid(this.carToUpdate);

    if(this.errors.length > 0)
    {
      return;
    }

    this.carsService.update(this.carToUpdate)
      .subscribe(
        () => {
          this.toastr.success('Car updated successfully', 'Success');
          this.router.navigate(['/cars/' + this.carToUpdate.id]);
        },
        (err) => {
          this.toastr.error(err, 'Error')
        }
      )
  }

  cancelUpdate() : void {
    this.location.back();
  }
}
