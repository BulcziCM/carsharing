import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-errors-list',
  inputs: ['errors'],
  templateUrl: './errors-list.component.html',
  styleUrls: ['./errors-list.component.scss']
})
export class ErrorsListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
