import { Component, OnInit } from '@angular/core';
import { UserAuthData } from '../models/user-auth-data';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { Md5 } from 'ts-md5/dist/md5';

import { LocalStorageService } from '../services/local-storage/local-storage.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  userName : string = '';
  password : string = '';

  constructor(
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService,
    private localStorageService: LocalStorageService) { }

  ngOnInit() {
  }

  logIn() : void {

    let userAuthData : UserAuthData = {
      userName : this.userName,
      password : Md5.hashStr(this.password).toString()
    }

    this.http.post<boolean>('http://localhost:10500/api/users/auth', userAuthData)
      .subscribe(
        (result) => {
          this.onAuthenticated(result);
        },
        () => {
          this.onAuthenticated(false);
        }
      );
  }

  onAuthenticated(isAuth : boolean) : void {
    this.localStorageService.save(isAuth)
    if(isAuth) {
      this.toastr.success('User authenticated successfully', 'Success');
      this.router.navigate(['']);
    }
    else {
      this.toastr.success('User loggon failed', 'Error');
    }
  }
}
