export class CarItem {
  id: number;
  make: string;
  model: string;
  licensePlate: string;
}
