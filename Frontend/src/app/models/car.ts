export class Car {
  id           : number
  make         : string
  model        : string
  licensePlate : string
  isAvailable  : boolean
  seatsCount   : number
  pricePerKM   : number
  pricePerDay  : number
  regionId     : number
}
