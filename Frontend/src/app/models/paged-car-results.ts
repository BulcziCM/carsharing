import { CarItem } from './car-item';

export class PagedCarResults {
  public page: number;
  public pageSize: number;
  public pagesCount: number;
  public resultsCount: number;
  public nextPageAttributes: string;
  public prevPageAttributes: string;
  public results: CarItem[] = [];
}
