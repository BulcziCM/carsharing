import { TestBed, async, inject } from '@angular/core/testing';

import { IsUserLoggedGuard } from './is-user-logged.guard';

describe('IsUserLoggedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsUserLoggedGuard]
    });
  });

  it('should ...', inject([IsUserLoggedGuard], (guard: IsUserLoggedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
