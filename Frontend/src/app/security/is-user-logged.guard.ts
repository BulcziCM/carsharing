import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from '../services/local-storage/local-storage.service';

@Injectable({
  providedIn: 'root'
})

export class IsUserLoggedGuard implements CanActivate {

  constructor(
    private router : Router,
    private localStorageService : LocalStorageService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean {
    debugger;
    let isAuthenticated : boolean = this.localStorageService.read();

    if(!isAuthenticated) {
      this.router.navigate(['auth']);
    }

    return isAuthenticated;
  }
}
