import { TestBed } from '@angular/core/testing';

import { CarItemValidatorService } from './car-item-validator.service';

describe('CarItemValidatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarItemValidatorService = TestBed.get(CarItemValidatorService);
    expect(service).toBeTruthy();
  });
});
