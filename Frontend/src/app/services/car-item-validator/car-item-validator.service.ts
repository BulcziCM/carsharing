import { Injectable } from '@angular/core';
import { CarItem } from 'src/app/models/car-item';

@Injectable({
  providedIn: 'root'
})

export class CarItemValidatorService {

  constructor() { }

  public isCarItemValid(carItem : CarItem) : string[] {
    let errors : string[] = [];

    if(!carItem.make || carItem.make.length === 0)
    {
      errors.push('Car make is missing');
    }

    if(!carItem.model || carItem.model.length === 0)
    {
      errors.push('Car model is missing');
    }

    if(!carItem.licensePlate || carItem.licensePlate.length <= 0 || carItem.licensePlate.length > 7)
    {
      errors.push('Car licence places are incorrect');
    }

    return errors;
  }
}
