import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Car } from 'src/app/models/car';
import { PagedCarResults } from 'src/app/models/paged-car-results';

@Injectable({
  providedIn: 'root'
})

export class CarsService {

  constructor(
    private http: HttpClient) { }

  public getCarsPage(pageAttributes : string) : Observable<PagedCarResults> {
    return this.http.get<PagedCarResults>('http://localhost:10500/api/cars/available/page' + pageAttributes);
  }

  public get(carId : number) : Observable<Car> {
    return this.http.get<Car>('http://localhost:10500/api/cars/' + carId);
  }

  public add(car : Car) : Observable<number> {
    return this.http.post<number>('http://localhost:10500/api/cars/', car);
  }

  public delete(carId : number) : Observable<any> {
    return this.http.delete<any>('http://localhost:10500/api/cars/' + carId);
  }

  public update(car : Car) : Observable<any> {
    return this.http.put<any>('http://localhost:10500/api/cars/' + car.id, car);
  }
}
