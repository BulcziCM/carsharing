import { Inject, Injectable  } from '@angular/core';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';

@Injectable()
export class LocalStorageService {

  private isAuthKey = 'carsharing_cm_isauth';

  constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService)
  {
  }

  save(val : boolean): void {
    this.storage.set(this.isAuthKey, val);
  }

  read() : boolean {
    var isAuth: boolean = this.storage.get(this.isAuthKey);
    return isAuth;
  }
}
